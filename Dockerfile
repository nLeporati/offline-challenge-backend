FROM gradle:7.4-jdk11

WORKDIR /app

COPY wrapper/ ./
COPY gradlew build.gradle settings.gradle gradlew.bat ./

RUN gradle wrapper

COPY src ./src


CMD ["./gradlew", "bootBuildImage"]