# Offline challenge

## Architecture

A structure derived from Hexagonal architecture is followed, the folders are:

- data: infrastructure layer to handle data through services and repositories.
- api: presentation layer with REST endpoints to externally consume the service.
- domain: business layer, this doesn't depend on the rest, its main entity are the UseCases, datasource's (data) and adapters (api) are available to handle communication with the other parts.

Some guidelines are:
- the database used is H2 in memory to speed up development, with Hibernate, also some sample data is loaded at startup.
- A controller advice is created to handle errors and not make visible to the outside.
- mappers are created for domain objects to not create dependency between others parts.

Due time limitations, many improvements can still be made, such as; logging, better exception handling, better validations, openapi, etc.

## Requirements

- Java 11

## Getting Started

To start the server, run:

```bash
./gradlew :bootRun
```

Open [http://localhost:8000](http://localhost:8000) to view H2 console.

Server runs on [http://localhost:8080](http://localhost:8080).

To check the unit tests run:
```bash
./gradlew :test
```
