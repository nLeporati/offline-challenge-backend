package cl.nleporati.offlinechallenge;

import java.math.BigDecimal;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import cl.nleporati.offlinechallenge.data.repository.ProductRepositoryJpa;
import cl.nleporati.offlinechallenge.data.repository.entity.ProductEntity;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class DataLoader implements ApplicationRunner {

    private final ProductRepositoryJpa productRepository;

    public void run(ApplicationArguments args) {
        productRepository.save(new ProductEntity("FAL-8406270", "500 Zapatilla Urbana Mujer", "NEW BALANCE", "37", BigDecimal.valueOf(42990.00), "https://falabella.scene7.com/is/image/Falabella/8406270_1", ""));
        productRepository.save(new ProductEntity("FAL-88195228", "Bicicleta Baltoro Aro 29r", "JEEP", "ST", BigDecimal.valueOf(399990.00), "https://falabella.scene7.com/is/image/Falabella/881952283_1", "https://falabella.scene7.com/is/image/Falabella/881952283_2"));
        productRepository.save(new ProductEntity("FAL-88189850", "Camisa Manga Corta Hombre", "BASEMENT", "M", BigDecimal.valueOf(24990.00), "https://falabella.scene7.com/is/image/Falabella/881898502_1", ""));
        productRepository.flush();
    }
}
