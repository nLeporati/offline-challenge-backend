package cl.nleporati.offlinechallenge.api.advice;

import cl.nleporati.offlinechallenge.api.model.ErrorResponse;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/error")
public class CustomErrorController implements ErrorController {

     @GetMapping
     public ResponseEntity<ErrorResponse> handleError() {
         ErrorResponse error = new ErrorResponse(HttpStatus.I_AM_A_TEAPOT, "error");
         return ResponseEntity.internalServerError().body(error);
     }

}
