package cl.nleporati.offlinechallenge.api.advice;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.server.ResponseStatusException;

import cl.nleporati.offlinechallenge.api.model.ErrorResponse;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
    
    @ExceptionHandler({ ResponseStatusException.class })
    protected ResponseEntity<ErrorResponse> handleStatusException(ResponseStatusException ex) {
        ErrorResponse error = new ErrorResponse(ex.getStatus(), ex.getMessage());
        return ResponseEntity.status(ex.getStatus()).body(error);
    }

    @ExceptionHandler({ Exception.class })
    protected ResponseEntity<ErrorResponse> handleCustomException() {
        ErrorResponse error = new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, "Application internal error");
        return ResponseEntity.internalServerError().body(error);
    }

}
