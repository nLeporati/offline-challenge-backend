package cl.nleporati.offlinechallenge.api.controller;

import java.util.List;

import cl.nleporati.offlinechallenge.domain.adapter.*;
import org.springframework.web.bind.annotation.*;

import cl.nleporati.offlinechallenge.api.mapper.ProductApiMapper;
import cl.nleporati.offlinechallenge.api.model.ProductListResponse;
import cl.nleporati.offlinechallenge.api.model.ProductRequest;
import cl.nleporati.offlinechallenge.api.model.ProductResponse;
import cl.nleporati.offlinechallenge.domain.model.Product;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/products")
@CrossOrigin("*")
public class ProductsController {

    private final GetProductsAdapter getProductsUseCase;

    private final CreateProductAdapter createProductUseCase;
    private final GetProductBySkuAdapter getProductBySkuUseCase;
    private final UpdateProductAdapter updateProductUseCase;
    private final DeleteProductAdapter deleteProductUseCase;

    @GetMapping
    public ProductListResponse list() {
        List<Product> products = getProductsUseCase.execute();
        return ProductApiMapper.toResponse(products);
    }

    @PostMapping
    public ProductResponse create(@RequestBody ProductRequest request) {
        Product product = ProductApiMapper.toDomain(null, request);
        Product newProduct = createProductUseCase.execute(product);
        return ProductApiMapper.toResponse(newProduct);
    }

    @GetMapping("{sku}")
    public ProductResponse get(@PathVariable String sku) {
        Product product = getProductBySkuUseCase.execute(sku);
        return ProductApiMapper.toResponse(product);
    }
    
    @PostMapping("{sku}")
    public ProductResponse update(@PathVariable String sku, @RequestBody ProductRequest body) {
        Product product = ProductApiMapper.toDomain(sku, body);
        Product updatedProduct = updateProductUseCase.execute(product);
        return ProductApiMapper.toResponse(updatedProduct);
    }

    @DeleteMapping("{sku}")
    public void delete(@PathVariable String sku) {
        deleteProductUseCase.execute(sku);
    }
}
