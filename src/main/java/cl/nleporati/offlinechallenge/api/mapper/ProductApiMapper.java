package cl.nleporati.offlinechallenge.api.mapper;

import java.util.List;
import java.util.stream.Collectors;

import cl.nleporati.offlinechallenge.api.model.ProductListResponse;
import cl.nleporati.offlinechallenge.api.model.ProductRequest;
import cl.nleporati.offlinechallenge.api.model.ProductResponse;
import cl.nleporati.offlinechallenge.domain.model.Product;
import lombok.experimental.UtilityClass;

@UtilityClass
public class ProductApiMapper {
    public static ProductResponse toResponse(Product product) {
        return ProductResponse.builder()
            .sku(product.getSku())
            .name(product.getName())
            .brand(product.getBrand())
            .size(product.getSize())
            .price(product.getPrice())
            .principalImage(product.getPrincipalImage())
            .otherImages(product.getOtherImages())
            .build();
    }
    
    public static ProductListResponse toResponse(List<Product> productList) {
        List<ProductResponse> list = productList.stream()
            .map(ProductApiMapper::toResponse)
            .collect(Collectors.toUnmodifiableList());

        return new ProductListResponse(list);
    }

    public static Product toDomain(String sku, ProductRequest request) {
        return Product.builder()
        .sku(sku)
        .name(request.getName())
        .brand(request.getBrand())
        .size(request.getSize())
        .price(request.getPrice())
        .principalImage(request.getPrincipalImage())
        .otherImages(request.getOtherImages())
        .build();
    }
}
