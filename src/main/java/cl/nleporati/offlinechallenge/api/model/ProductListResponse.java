package cl.nleporati.offlinechallenge.api.model;

import java.util.List;
import lombok.Value;

@Value
public class ProductListResponse {
    private List<ProductResponse> products;
}
