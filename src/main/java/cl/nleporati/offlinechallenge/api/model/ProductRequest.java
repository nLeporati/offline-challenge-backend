package cl.nleporati.offlinechallenge.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import lombok.Data;

@Data
public class ProductRequest implements Serializable {
    private String name;
    private String brand;
    private String size;
    private BigDecimal price;
    private String principalImage;
    private List<String> otherImages;
}
