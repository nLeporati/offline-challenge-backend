package cl.nleporati.offlinechallenge.api.model;

import java.math.BigDecimal;
import java.util.List;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class ProductResponse {
    private String sku;
    private String name;
    private String brand;
    private String size;
    private BigDecimal price;
    private String principalImage;
    private List<String> otherImages;
}
