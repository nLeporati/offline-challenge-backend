package cl.nleporati.offlinechallenge.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.nleporati.offlinechallenge.data.repository.entity.ProductEntity;

public interface ProductRepositoryJpa extends JpaRepository<ProductEntity, String> {
    
}
