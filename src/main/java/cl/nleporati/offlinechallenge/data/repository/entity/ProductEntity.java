package cl.nleporati.offlinechallenge.data.repository.entity;

import java.math.BigDecimal;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

@Data
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
@Entity(name = "product")
public class ProductEntity {
    @Id
    private String sku;
    @NotNull
    @Length(min=3, max=50)
    private String name;
    @NotNull
    @Length(min=3, max=50)
    private String brand;
    @NotEmpty
    private String size;
    @NotNull
    @Range(min=1, max=99999999)
    private BigDecimal price;
    @NotNull
    @NotEmpty
    private String principalImage;
    private String otherImages;
}
