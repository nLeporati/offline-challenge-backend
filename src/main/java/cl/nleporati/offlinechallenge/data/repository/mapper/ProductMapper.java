package cl.nleporati.offlinechallenge.data.repository.mapper;

import cl.nleporati.offlinechallenge.data.repository.entity.ProductEntity;
import cl.nleporati.offlinechallenge.domain.model.Product;
import lombok.experimental.UtilityClass;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@UtilityClass
public class ProductMapper {

    private static final String COMMA = ",";
    public static Product toDomain(ProductEntity entity) {
        List<String> otherImages = entity.getOtherImages().isEmpty()
                ? Collections.emptyList()
                : Arrays.stream(entity.getOtherImages().split(COMMA)).collect(Collectors.toList());
        return Product.builder()
            .sku(entity.getSku())
            .name(entity.getName())
            .brand(entity.getBrand())
            .size(entity.getSize())
            .price(entity.getPrice())
            .principalImage(entity.getPrincipalImage())
            .otherImages(otherImages)
            .build();
    }

    public static ProductEntity toEntity(Product product) {
        String otherImages = String.join(COMMA, product.getOtherImages());
        return ProductEntity.builder()
            .sku(product.getSku())
            .name(product.getName())
            .brand(product.getBrand())
            .size(product.getSize())
            .price(product.getPrice())
            .principalImage(product.getPrincipalImage())
            .otherImages(otherImages)
            .build();
    }
}
