package cl.nleporati.offlinechallenge.data.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import cl.nleporati.offlinechallenge.data.repository.ProductRepositoryJpa;
import cl.nleporati.offlinechallenge.data.repository.entity.ProductEntity;
import cl.nleporati.offlinechallenge.data.repository.mapper.ProductMapper;
import cl.nleporati.offlinechallenge.domain.datasource.ProductDatasource;
import cl.nleporati.offlinechallenge.domain.exception.ProductNotFoundException;
import cl.nleporati.offlinechallenge.domain.model.Constants;
import cl.nleporati.offlinechallenge.domain.model.Product;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ProductService implements ProductDatasource {

    private final ProductRepositoryJpa productRepository;

    @Override
    public Product get(String id) {
        ProductEntity entity = productRepository.findById(id)
            .orElseThrow(ProductNotFoundException::new);

        return ProductMapper.toDomain(entity);
    }

    @Override
    public Product update(Product product) {
        if (this.exists(product.getSku())) {
            ProductEntity entity = ProductMapper.toEntity(product);
            return ProductMapper.toDomain(productRepository.saveAndFlush(entity));
        } else {
            throw new ProductNotFoundException();
        }
    }

    @Override
    public List<Product> list() {
        List<ProductEntity> list = productRepository.findAll();
        return list.stream()
            .map(ProductMapper::toDomain)
            .collect(Collectors.toList());
    }

    @Override
    public void delete(String id) {
        if (this.exists(id)) {
            productRepository.deleteById(id);
        } else {
            throw new ProductNotFoundException();
        }
    }

    @Override
    public Product create(Product product) {
        if (this.exists(product.getSku())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Constants.MSG_PRODUCT_EXISTS);
        } else {
            ProductEntity entity = ProductMapper.toEntity(product);
            ProductEntity newEntity = productRepository.saveAndFlush(entity);
            return ProductMapper.toDomain(newEntity);
        }
    }

    private boolean exists(String id) {
        return productRepository.existsById(id);
    }
}
