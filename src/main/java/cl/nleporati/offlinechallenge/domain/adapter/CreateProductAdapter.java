package cl.nleporati.offlinechallenge.domain.adapter;

import cl.nleporati.offlinechallenge.domain.model.Product;

public interface CreateProductAdapter {
    Product execute(Product product);
}
