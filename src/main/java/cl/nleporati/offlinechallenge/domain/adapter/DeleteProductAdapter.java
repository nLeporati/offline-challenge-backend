package cl.nleporati.offlinechallenge.domain.adapter;

public interface DeleteProductAdapter {
    void execute(final String sku);
}
