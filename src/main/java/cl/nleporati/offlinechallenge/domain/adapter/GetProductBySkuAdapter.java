package cl.nleporati.offlinechallenge.domain.adapter;

import cl.nleporati.offlinechallenge.domain.model.Product;

public interface GetProductBySkuAdapter {
    Product execute(final String sku);
}
