package cl.nleporati.offlinechallenge.domain.adapter;

import java.util.List;

import cl.nleporati.offlinechallenge.domain.model.Product;

public interface GetProductsAdapter {
    List<Product> execute();
}
