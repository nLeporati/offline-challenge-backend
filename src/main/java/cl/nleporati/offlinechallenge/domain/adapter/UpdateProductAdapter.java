package cl.nleporati.offlinechallenge.domain.adapter;

import cl.nleporati.offlinechallenge.domain.model.Product;

public interface UpdateProductAdapter {
    Product execute(Product product);
}
