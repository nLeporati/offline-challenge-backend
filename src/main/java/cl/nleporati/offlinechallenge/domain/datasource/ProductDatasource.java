package cl.nleporati.offlinechallenge.domain.datasource;

import java.util.List;

import cl.nleporati.offlinechallenge.domain.model.Product;

public interface ProductDatasource {
    Product get(String id);
    Product update(Product product);
    Product create(Product product);
    List<Product> list();
    void delete(String id);
}
