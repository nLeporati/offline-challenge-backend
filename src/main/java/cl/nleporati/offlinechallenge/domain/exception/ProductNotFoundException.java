package cl.nleporati.offlinechallenge.domain.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import cl.nleporati.offlinechallenge.domain.model.Constants;

public class ProductNotFoundException extends ResponseStatusException {

    public ProductNotFoundException() {
        super(HttpStatus.NOT_FOUND, Constants.MSG_PRODUCT_NOT_FOUND);
    }
    
}
