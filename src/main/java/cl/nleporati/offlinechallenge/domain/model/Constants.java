package cl.nleporati.offlinechallenge.domain.model;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Constants {
    public static final String MSG_PRODUCT_NOT_FOUND = "Product not found";
    public static final String MSG_PRODUCT_EXISTS = "Product already exists";
}
