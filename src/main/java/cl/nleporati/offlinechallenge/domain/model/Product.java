package cl.nleporati.offlinechallenge.domain.model;

import java.math.BigDecimal;
import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Product {
    private String sku;
    private String name;
    private String brand;
    private String size;
    private BigDecimal price;
    private String principalImage;
    private List<String> otherImages;
}
