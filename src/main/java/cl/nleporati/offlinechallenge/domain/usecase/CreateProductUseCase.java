package cl.nleporati.offlinechallenge.domain.usecase;

import org.springframework.stereotype.Component;

import cl.nleporati.offlinechallenge.domain.adapter.CreateProductAdapter;
import cl.nleporati.offlinechallenge.domain.datasource.ProductDatasource;
import cl.nleporati.offlinechallenge.domain.model.Product;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class CreateProductUseCase implements CreateProductAdapter {
    
    private final ProductDatasource productDatasource;
    
    @Override
    public Product execute(Product product) {
        return productDatasource.create(product);
    }
    
}
