package cl.nleporati.offlinechallenge.domain.usecase;

import org.springframework.stereotype.Component;

import cl.nleporati.offlinechallenge.domain.adapter.DeleteProductAdapter;
import cl.nleporati.offlinechallenge.domain.datasource.ProductDatasource;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class DeleteProductUseCase implements DeleteProductAdapter {

    private final ProductDatasource productDatasource;
    
    @Override
    public void execute(String sku) {
        productDatasource.delete(sku);
    }
    
}
