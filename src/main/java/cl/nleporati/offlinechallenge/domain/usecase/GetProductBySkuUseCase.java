package cl.nleporati.offlinechallenge.domain.usecase;

import org.springframework.stereotype.Component;

import cl.nleporati.offlinechallenge.domain.adapter.GetProductBySkuAdapter;
import cl.nleporati.offlinechallenge.domain.datasource.ProductDatasource;
import cl.nleporati.offlinechallenge.domain.model.Product;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class GetProductBySkuUseCase implements GetProductBySkuAdapter {

    private final ProductDatasource productDatasource;

    @Override
    public Product execute(String sku) {
        return productDatasource.get(sku);
    }
    
}
