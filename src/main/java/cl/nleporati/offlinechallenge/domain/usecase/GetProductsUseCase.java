package cl.nleporati.offlinechallenge.domain.usecase;

import java.util.List;

import org.springframework.stereotype.Component;

import cl.nleporati.offlinechallenge.domain.adapter.GetProductsAdapter;
import cl.nleporati.offlinechallenge.domain.datasource.ProductDatasource;
import cl.nleporati.offlinechallenge.domain.model.Product;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class GetProductsUseCase implements GetProductsAdapter {

    private final ProductDatasource productDatasource;

    @Override
    public List<Product> execute() {
        return productDatasource.list();
    }
    
}
