package cl.nleporati.offlinechallenge.api.advice;

import cl.nleporati.offlinechallenge.api.model.ErrorResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@ExtendWith(MockitoExtension.class)
public class CustomErrorControllerTest {
    @InjectMocks
    private CustomErrorController controller;

    @Test
    void handleErrorOk() {
        ResponseEntity<ErrorResponse> response = controller.handleError();
        Assertions.assertNotNull(response.getBody());
        Assertions.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
    }
}
