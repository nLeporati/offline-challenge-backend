package cl.nleporati.offlinechallenge.api.advice;

import cl.nleporati.offlinechallenge.api.model.ErrorResponse;
import cl.nleporati.offlinechallenge.domain.exception.ProductNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@ExtendWith(MockitoExtension.class)
public class RestExceptionHandlerTest {
    @InjectMocks
    private RestExceptionHandler handler;

    @Test
    void handleStatusExceptionOk() {
        ResponseEntity<ErrorResponse> error = handler.handleStatusException(new ProductNotFoundException());
        Assertions.assertEquals(HttpStatus.NOT_FOUND, error.getStatusCode());
    }

    @Test
    void handleExceptionOk() {
        ResponseEntity<ErrorResponse> error = handler.handleCustomException();
        Assertions.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, error.getStatusCode());
    }
}
