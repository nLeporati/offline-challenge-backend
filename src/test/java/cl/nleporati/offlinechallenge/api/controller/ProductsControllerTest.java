package cl.nleporati.offlinechallenge.api.controller;

import cl.nleporati.offlinechallenge.api.model.ProductListResponse;
import cl.nleporati.offlinechallenge.api.model.ProductRequest;
import cl.nleporati.offlinechallenge.api.model.ProductResponse;
import cl.nleporati.offlinechallenge.domain.model.Product;
import cl.nleporati.offlinechallenge.domain.usecase.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Collections;

import static org.mockito.Mockito.*;
import static cl.nleporati.offlinechallenge.TestingUtils.*;

@ExtendWith(MockitoExtension.class)
public class ProductsControllerTest {
    @InjectMocks
    private ProductsController controller;
    @Mock
    private GetProductsUseCase getProductsUseCase;
    @Mock
    private CreateProductUseCase createProductUseCase;
    @Mock
    private GetProductBySkuUseCase getProductBySkuUseCase;
    @Mock
    private UpdateProductUseCase updateProductUseCase;
    @Mock
    private DeleteProductUseCase deleteProductUseCase;

    private Product product;

    @BeforeEach
    void init() {
        product = Product.builder().name("any").brand("any").price(BigDecimal.ONE)
                .size("1").sku(SKU).principalImage("any").otherImages(Collections.emptyList())
                .build();
    }

    @Test
    void listOk() {
        when(getProductsUseCase.execute()).thenReturn(Collections.singletonList(product));
        ProductListResponse response = controller.list();
        Assertions.assertFalse(response.getProducts().isEmpty());
    }

    @Test
    void getOk() {
        when(getProductBySkuUseCase.execute(any())).thenReturn(product);
        ProductResponse response = controller.get(SKU);
        Assertions.assertNotNull(response);
    }

    @Test
    void createOk() {
        ProductRequest request = new ProductRequest();
        when(createProductUseCase.execute(any())).thenReturn(product);
        ProductResponse response = controller.create(request);
        Assertions.assertNotNull(response);
    }

    @Test
    void updateOk() {
        ProductRequest request = new ProductRequest();
        when(updateProductUseCase.execute(any())).thenReturn(product);
        ProductResponse response = controller.update(SKU, request);
        Assertions.assertNotNull(response);
    }

    @Test
    void deleteOk() {
        doNothing().when(deleteProductUseCase).execute(any());
        controller.delete(SKU);
        verify(deleteProductUseCase, times(ONE)).execute(SKU);
    }
}
