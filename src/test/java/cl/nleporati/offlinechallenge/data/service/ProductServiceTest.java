package cl.nleporati.offlinechallenge.data.service;

import cl.nleporati.offlinechallenge.data.repository.ProductRepositoryJpa;
import cl.nleporati.offlinechallenge.data.repository.entity.ProductEntity;
import cl.nleporati.offlinechallenge.domain.exception.ProductNotFoundException;
import cl.nleporati.offlinechallenge.domain.model.Constants;
import cl.nleporati.offlinechallenge.domain.model.Product;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static cl.nleporati.offlinechallenge.TestingUtils.*;

@ExtendWith(MockitoExtension.class)
public class ProductServiceTest {
    @InjectMocks
    private ProductService service;
    @Mock
    private ProductRepositoryJpa repository;

    private ProductEntity entity;
    private Product product;

    @BeforeEach
    void init() {
        entity = ProductEntity.builder().name("any").brand("any").price(BigDecimal.ONE)
            .size("1").sku(SKU).principalImage("any").otherImages("")
            .build();
        product = Product.builder().name("any").brand("any").price(BigDecimal.ONE)
            .size("1").sku(SKU).principalImage("any").otherImages(Collections.emptyList())
            .build();
    }

    @Test
    void getOk() {
        when(repository.findById(any())).thenReturn(Optional.of(entity));
        Product product = service.get(SKU);
        Assertions.assertNotNull(product);
    }

    @Test
    void getNotFound() {
        ProductNotFoundException ex = Assertions.assertThrows(ProductNotFoundException.class, () -> {
            service.get(SKU);
        });
        Assertions.assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
    }

    @Test
    void updateOk() {
        when(repository.existsById(any())).thenReturn(true);
        when(repository.saveAndFlush(any())).thenReturn(entity);
        Product newProduct = service.update(product);
        Assertions.assertNotNull(newProduct);
    }

    @Test
    void updateNotFound() {
        when(repository.existsById(any())).thenReturn(false);
        ProductNotFoundException ex = Assertions.assertThrows(ProductNotFoundException.class, () -> {
            service.update(product);
        });
        Assertions.assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
    }

    @Test
    void listOk() {
        when(repository.findAll()).thenReturn(Collections.singletonList(entity));
        List<Product> list = service.list();
        Assertions.assertFalse(list.isEmpty());
    }

    @Test
    void deleteOk() {
        when(repository.existsById(any())).thenReturn(true);
        doNothing().when(repository).deleteById(any());
        service.delete(SKU);
        verify(repository, times(ONE)).existsById(any());
        verify(repository, times(ONE)).deleteById(any());
    }

    @Test
    void deleteNotFound() {
        when(repository.existsById(any())).thenReturn(false);
        ProductNotFoundException ex = Assertions.assertThrows(ProductNotFoundException.class, () -> {
            service.delete(SKU);
        });
        Assertions.assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
    }

    @Test
    void createOk() {
        when(repository.existsById(any())).thenReturn(false);
        when(repository.saveAndFlush(any())).thenReturn(entity);
        Product newProduct = service.create(product);
        Assertions.assertNotNull(newProduct);
    }

    @Test
    void createBadRequest() {
        when(repository.existsById(any())).thenReturn(true);
        ResponseStatusException ex = Assertions.assertThrows(ResponseStatusException.class, () -> {
            service.create(product);
        });
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, ex.getStatus());
    }
}
