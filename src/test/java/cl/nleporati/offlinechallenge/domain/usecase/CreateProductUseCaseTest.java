package cl.nleporati.offlinechallenge.domain.usecase;

import cl.nleporati.offlinechallenge.domain.datasource.ProductDatasource;
import cl.nleporati.offlinechallenge.domain.model.Product;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CreateProductUseCaseTest {
    @InjectMocks
    private CreateProductUseCase useCase;
    @Mock
    private ProductDatasource datasource;

    @Test
    void createProductOk() {
        when(datasource.create(any())).thenReturn(mock(Product.class));
        Product newProduct = useCase.execute(Product.builder().build());
        Assertions.assertNotNull(newProduct);
    }
}
