package cl.nleporati.offlinechallenge.domain.usecase;

import cl.nleporati.offlinechallenge.domain.datasource.ProductDatasource;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;
import static cl.nleporati.offlinechallenge.TestingUtils.*;

@ExtendWith(MockitoExtension.class)
public class DeleteProductUseCaseTest {
    @InjectMocks
    private DeleteProductUseCase useCase;
    @Mock
    private ProductDatasource datasource;

    @Test
    void deleteProductOk() {
        doNothing().when(datasource).delete(any());
        useCase.execute(SKU);
        verify(datasource, times(ONE)).delete(any());
    }
}
