package cl.nleporati.offlinechallenge.domain.usecase;

import cl.nleporati.offlinechallenge.domain.datasource.ProductDatasource;
import cl.nleporati.offlinechallenge.domain.model.Product;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import static cl.nleporati.offlinechallenge.TestingUtils.*;

@ExtendWith(MockitoExtension.class)
public class GetProductBySkuUseCaseTest {
    @InjectMocks
    private GetProductBySkuUseCase useCase;
    @Mock
    private ProductDatasource datasource;

    @Test
    void getProductOk() {
        when(datasource.get(any())).thenReturn(mock(Product.class));
        Product newProduct = useCase.execute(SKU);
        Assertions.assertNotNull(newProduct);
    }
}
