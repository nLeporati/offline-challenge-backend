package cl.nleporati.offlinechallenge.domain.usecase;

import cl.nleporati.offlinechallenge.domain.datasource.ProductDatasource;
import cl.nleporati.offlinechallenge.domain.model.Product;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class GetProductsUseCaseTest {
    @InjectMocks
    private GetProductsUseCase useCase;
    @Mock
    private ProductDatasource datasource;

    @Test
    void listProductOk() {
        when(datasource.list()).thenReturn(Collections.emptyList());
        List<Product> list = useCase.execute();
        Assertions.assertTrue(list.isEmpty());
    }
}
